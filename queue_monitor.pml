#define N 10
int back = 0;
int items[N];

#define N_DATA 2
int data[N_DATA];

#define LIN_LEN (2*N_DATA + N_DATA)
int linearization[LIN_LEN];
int linearization_index = 0;

#define commit(v) \
  linearization[linearization_index] = v -> linearization_index++

bool ready = false;
byte done;

active [N_DATA] proctype to_enqueue()
{
  ready;
  assert(_pid >= 0 && _pid < N_DATA);
  int v = data[_pid];

  byte i;
  d_step { i = back; back++; commit(v) }
  d_step { items[i] = v; commit(v) }
  done++
}

active [N_DATA] proctype to_dequeue()
{
  ready;

  int range;
  int i;
  int value;
again:
  range = back - 1;
  if
  :: range >= N -> range = N-1
  :: else       -> skip
  fi
  for (i : 0 .. range) {
    d_step {
      value = items[i]
      items[i] = 0
      if
      :: value != 0 -> commit(-value)
      :: else
      fi
    }
    if
    :: value != 0 -> goto exit
    :: else
    fi
  }
  goto again;
exit:
  done++
}

active proctype moniter()
{
  done == 2 * N_DATA
  d_step {
    int stack[N_DATA] = 0;
    int i,j;
    int v;

    /* property 1. Only dequeue elements that have been enqueued */
    /* property 2. Enqueue should respect dequeue for each pair */

    int out[N_DATA] = 0
    int out_index = 0;

    for (i in linearization) {
      v = linearization[i];
      if
      :: v < 0 -> out[out_index] = -v; out_index++
      :: else  -> skip
      fi
    }

    assert(out_index == N_DATA);

    for (j : 0 .. out_index - 2)  {
      byte cur = j
      for (i in linearization) {
        if
        :: cur < j+2 && linearization[i] == out[cur] -> cur++
        :: else -> skip
        fi
      }
      assert(cur == j+2);
    }
  }
}

init {
  d_step {
    int i;
    for (i : 1 .. N_DATA) {
      data[i-1] = i;
    }
    ready = true;
  }
}
