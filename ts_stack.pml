#define threads 4
#define tl_pool_size 2

typedef Cell {
  int value
  int timestamp
  bool deleted
}

typedef ThreadLocal {
  byte index
  Cell pool[tl_pool_size]
}

ThreadLocal pools[threads]
#define tl pools[_pid]

#define MIN(X, Y) (X > Y -> Y : X)
#define MAX(X, Y) (X > Y -> X : Y)

inline CAS(R, X, Y, Z)
{
  if
  :: X == Y -> R = true; X = Z
  :: else -> R = false
  fi
}

#define ts_bottom 0
byte timestamp = 1
#define get_timestamp(left) d_step { left = timestamp -> timestamp ++ }

int history[2*threads]
byte history_index
byte finished

#define commit(x) {history[history_index] = x -> history_index++}

active [threads/2] proctype to_push()
{
  int data = 1 + _pid
  byte ts
  byte old_index

  d_step {
    old_index = tl.index
    tl.pool[tl.index].value = data
    tl.pool[tl.index].deleted = false
    tl.pool[tl.index].timestamp = 0
    tl.index = tl.index + 1
    assert(tl.index <= tl_pool_size)
    commit(data)
  }

  get_timestamp(ts)

  d_step{
    tl.pool[old_index].timestamp = ts
    commit(data)
  }
  finished++
}

active [threads/2] proctype to_pop()
{
  int i
  int index
  int value
  bool ret
  int max_i
  int max_index
  int max_ts
  int cur_ts

  byte history_index_pre
  d_step {
    history_index_pre = history_index
    history_index++
  }
start:
  max_ts = 0
  max_index = 0
  max_i = 0
  for (i in pools) {
    d_step {
      index = pools[i].index
      if
      :: index > 0 -> cur_ts = pools[i].pool[index-1].timestamp
      :: else cur_ts = -1
      fi
    }

    if
    :: cur_ts == ts_bottom ->
        atomic {
          assert(index > 0)
          CAS(ret, pools[i].pool[index-1].deleted, false, 1)
          if
          :: ret ->
            value = pools[i].pool[index-1].value
            history[history_index_pre] = -value
            commit(-value)
            pools[i].index--
            goto exit
          :: else
          fi
        }
    :: cur_ts > max_ts ->
        max_ts = cur_ts
        max_index = index
        max_i = i
    :: else
    fi
  }

  atomic {
    if
    :: max_index != 0 && pools[max_i].index == max_index ->
        atomic {
          CAS(ret, pools[max_i].pool[max_index-1].deleted, false, true)
          if
          :: ret ->
              value = pools[max_i].pool[max_index-1].value
              history[history_index_pre] = -value
              commit(-value)
              pools[max_i].index--
          :: else
          fi
        }
    :: else -> goto start
    fi
  }

exit:
  finished++
}

active proctype monitor()
{
  finished == threads
  d_step{
    int i,j
    int v
    /* v-1 -> index */
    int out_dict_1[threads/2] = -1
    int out_dict_2[threads/2] = -1

    {
      /* property 1. only pop elements that have been pushed; use the second
       * pop commit point */
      for (i in history) {
        v = history[i]
        if
        :: v < 0 ->
          if
          :: out_dict_1[-v-1] == -1 -> out_dict_1[-v-1] = i
          :: else ->
              assert(out_dict_2[-v-1] == -1)
              out_dict_2[-v-1] = i
          fi
        :: else
        fi
      }

      for (i in out_dict_2) {
        for (j : 0 .. out_dict_2[i]-1) {
          if
          :: history[j] == i+1 -> goto next
          :: else
          fi
        }
        assert(0)
  next:
      }
    }

    {
      /* property 2. each element is only popped once */
      byte popped[threads/2]
      for (i in history) {
        v = history[i]
        if
        :: v < 0 -> popped[-v-1]++
        :: else
        fi
      }
      for (i in popped) {
        assert(popped[i] == 2)
      }
    }

    {
      /* property 3. FILO order for non-interleaving pairs */
      int out_index
      int out_data[threads/2]

      /* v-1 -> index */
      int out_dict[threads/2]

      for (i in history) {
        v = history[i]
        if
        :: v < 0 && out_dict_2[-v-1] == i ->
            out_data[out_index] = -v
            out_index++
        :: else
        fi
      }

      assert(out_index == threads/2)

      byte before
      int ins[4]
      int v1, v2

      for (j : 0 .. out_index-2) {
        before = 0
        v1 = out_data[j]
        v2 = out_data[j+1]

        for (i : 0 .. MIN(out_dict_2[v1-1], out_dict_2[v2-1])-1) {
          if
          :: history[i] == v1 || history[i] == v2 -> ins[before] = i; before++
          :: else
          fi
        }

        if
        :: before == 4 &&
          ins[3] < MIN(out_dict_1[v1-1], out_dict_1[v2-1]) &&
          history[ins[0]] == history[ins[1]] &&
          history[ins[2]] == history[ins[3]] ->
           assert(history[ins[0]] == v2)
           assert(history[ins[2]] == v1)
        :: else
        fi
      }
    }
  }
}
