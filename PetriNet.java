import java.util.*;
import java.util.stream.Collectors;

class PetriNet {
    public static void main(String[] args) {
        State init = State.of("p3", "p3");
        List<State> visited = new ArrayList<>(Arrays.asList(init));
        Graph g = Graph.buildGraph();
        g.reverse();
        Queue<State> frontier = new LinkedList<>(Arrays.asList(init));
        while (!frontier.isEmpty()) {
            State s = frontier.poll();
            System.out.println("Expanding: " + s);
            List<State> before = g.step(s);
            System.out.println("Before filter: " + before);
            List<State> after = State.updateVisited(visited, before);
            System.out.println("After filter:" + after + "\n");
            frontier.addAll(after);
        }
        System.out.println("Reachable: " + visited);
    }
}

class State {
    Map<String, Integer> dict = new HashMap<>();

    private static boolean contains(List<State> visited, State s) {
        return visited.stream().anyMatch(it -> it.le(s));
    }

    static List<State> updateVisited(List<State> visited, List<State> states) {
        List<State> new_states = new ArrayList<>();
        for (State s : states) {
            if (!contains(visited, s)) {
                new_states.add(s);
                visited.add(s);
            }
        }
        return new_states;
    }

    static State of(String... list) {
        State ret = new State();
        for (String node : list) {
            ret.dict.compute(node, (k, old) -> old == null ? 1 : old + 1);
        }
        return ret;
    }

    boolean le(State other) {
        boolean ret = true;
        for (Map.Entry<String, Integer> entry : dict.entrySet()) {
            String node = entry.getKey();
            int count = entry.getValue();
            if (count > other.dict.getOrDefault(node, 0)) {
                ret = false;
                break;
            }
        }
        return ret;
    }

    public String toString() {
        List<String> list = new ArrayList<>();
        dict.forEach(
                (node, count) -> list.addAll(Collections.nCopies(count, node)));
        return list.toString();
    }

    public State clone() {
        State s = new State();
        s.dict = new HashMap<>(this.dict);
        return s;
    }
}

class Graph {
    List<Action> actions;

    static Graph buildGraph() {
        Graph g = new Graph();
        List<Action> actions = new ArrayList<>();
        actions.add(
                new Action("t1",
                        Arrays.asList("p1"),
                        Arrays.asList("p1", "p2")
                )
        );
        actions.add(
                new Action("t2",
                        Arrays.asList("p1", "p2"),
                        Arrays.asList("p3")
                )
        );
        actions.add(
                new Action("t1",
                        Arrays.asList("p3"),
                        Arrays.asList("p1")
                )
        );
        g.actions = actions;
        return g;
    }

    void reverse() {
        actions.forEach((a) -> {
            List<String> t = a.ins;
            a.ins = a.outs;
            a.outs = t;
        });
    }

    List<State> step(State s) {
        return actions
                .stream().map(a -> step(s, a)).collect(Collectors.toList());
    }

    private State step(State old_s, Action a) {
        State s = old_s.clone();
        for (String node : a.ins) {
            s.dict.computeIfPresent(node, (k, old) -> Math.max(old - 1, 0));
        }
        for (String node : a.outs) {
            s.dict.compute(node, (k, old) -> old == null ? 1 : old + 1);
        }
        return s;
    }

    static class Action {
        String name;
        List<String> ins;
        List<String> outs;

        Action(String name, List<String> ins, List<String> outs) {
            this.name = name;
            this.ins = ins;
            this.outs = outs;
        }
    }
}
