#define N 10
int back = 0;
int items[N];

#define N_DATA 3
int data[N_DATA];

bool ready = false;

active [N_DATA] proctype to_enqueue()
{
  ready;
  assert(_pid >= 0 && _pid < N_DATA);
  int v = data[_pid];

  byte i;
  d_step { i = back; back++ }
  items[i] = v;
}

active [N_DATA] proctype to_dequeue()
{
  ready;

  int range;
  int i;
  int value;
again:
  range = back - 1;
    if
    :: range >= N -> range = 8
    :: else          skip
    fi
    for (i : 0 .. range) {
      d_step { value = items[i]; items[i] = 0 }
      if
      :: value != 0 -> goto exit
      :: else
      fi
    }
  goto again;
exit:
}

init {
  d_step {
    int i;
    for (i : 1 .. N_DATA) {
      data[i-1] = i;
    }
    ready = true;
  }
}
