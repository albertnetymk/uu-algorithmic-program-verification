.PHONY: default
default: clean

.PHONY: run
run:
	# spin -run -O queue.pml
	# spin -run -O queue_monitor.pml
	spin -run -O ts_stack.pml

.PHONY: clean
clean:
	rm -f a.out pan *.trail
	rm -f *.class
